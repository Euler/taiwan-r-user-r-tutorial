#! /usr/bin/Rscript
argv <- commandArgs(TRUE)
require(knitr)
opts_knit$set(upload.fun = image_uri)
knit(argv[1], argv[2])